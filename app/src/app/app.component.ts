import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor() {
  }

  public title = 'Make me coffee';

  public myColor = "#000";
  public isBoss: boolean = false;

}
