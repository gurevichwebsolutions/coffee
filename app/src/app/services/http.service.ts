import { Injectable } from '@angular/core';
import { Order } from '../interfaces/order';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User} from '../interfaces/user';


@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  public registerOrder = (order: Order) => {
    return this.http.get('http://localhost:3000/order',
      {params: {user: order.user, delay: order.delay.toString(), priority: order.priority.toString()}});
  }

  public getOrderStatus = (waited: string) => {
    return this.http.get('http://localhost:3000/polling', { params: {waited}, observe: 'response' });
  }

  public getOrdersFromDb = () => {
    return this.http.get('http://localhost:3000/getorders');
  }


  public getUsersList = (): Observable<User[]> => {
    return this.http.get<User[]>('assets/data/users.json').pipe(map(
      (users) => {
        return users.map(user => {
          return user;
        });
      }
    ));
  }




  // const clicks = fromEvent(document, 'click');
  // const positions = clicks.pipe(map(ev => ev.clientX));
  // positions.subscribe(x => console.log(x));
}
