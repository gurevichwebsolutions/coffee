import { Component, OnInit } from '@angular/core';
import { BottomWindowComponent } from "../bottom-window/bottom-window.component";
import { MatBottomSheet } from "@angular/material/bottom-sheet";
import {SharedService} from "../../services/shared.service";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  constructor(private _bottomSheet: MatBottomSheet,
              private shared: SharedService) {
    this.shared.change.subscribe(data => {
      if (data) {
        this.ordersList[data.id] = data;
      }

      // console.log(this.ordersList);
    });
  }

  ngOnInit(): void {
  }

  public ordersList: any = {};

  public openBottomWindow = (): void => {
    this._bottomSheet.open(BottomWindowComponent);
  }

}
