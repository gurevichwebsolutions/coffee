import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { ExcelService } from '../../services/excel.service';
import { User } from '../../interfaces/user';

@Component({
  selector: 'app-histogram',
  templateUrl: './histogram.component.html',
  styleUrls: ['./histogram.component.scss']
})
export class HistogramComponent implements OnInit {

  constructor(private http: HttpService, private excelService: ExcelService) { }

  public users: User[] = [];
  public initOpts;
  public options;
  public orders;

  ngOnInit(): void {

    this.http.getOrdersFromDb().subscribe((res: any[]) => {
      res.map((order) => {
        const exists = this.users.some(user => user.name === order.user);
        if (!exists) {
          this.users.push({name: order.user, count: 1});
        }else{
          this.users.map(user => {
            if (user.name === order.user) {
              user.count++;
            }
          });
        }
      });
      this.initHistogram();
    });
  }

  get usersNames(): string[]{
    return this.users.map(user => user.name);
  }

  get usersCups(): number[]{
    return this.users.map(user => user.count);
  }

  public initHistogram = (): void => {

    this.initOpts = {
      renderer: 'svg',
      width: 300,
      height: 300
    };

    this.options = {
      color: ['#ff4081'],
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow'
        }
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      xAxis: [
        {
          type: 'category',
          data: this.usersNames,
          axisTick: {
            alignWithLabel: true
          }
        }
      ],
      yAxis: [{
        type: 'value'
      }],
      series: [{
        name: 'Cups',
        type: 'bar',
        barWidth: '60%',
        data: this.usersCups
      }]
    };
  }

  public downloadReport = (): void => {

    const data = this.users.map( el => {
      return {'Employee name': el.name, 'Number of coffee cups': el.count};
    });

    this.excelService.exportAsExcelFile(data, 'Month Report');
  }
}
