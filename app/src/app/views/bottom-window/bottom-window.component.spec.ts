import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BottomWindowComponent } from './bottom-window.component';

describe('BottomWindowComponent', () => {
  let component: BottomWindowComponent;
  let fixture: ComponentFixture<BottomWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BottomWindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BottomWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
