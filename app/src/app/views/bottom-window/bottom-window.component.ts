import { Component, OnInit } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { Order } from '../../interfaces/order';
import { HttpService } from '../../services/http.service';
import { interval, Subscription } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';
import { SharedService } from '../../services/shared.service';

interface Data extends Object{
  job: {
    id: string;
  };
}

@Component({
  selector: 'app-bottom-window',
  templateUrl: './bottom-window.component.html',
  styleUrls: ['./bottom-window.component.scss']
})
export class BottomWindowComponent implements OnInit {
  constructor(
    private _bottomSheetRef: MatBottomSheetRef<BottomWindowComponent>,
    private http: HttpService,
    private shared: SharedService) {}

  public user: string;
  public delay: string;
  public priority: number;
  private order: Order;
  public orderComplete = false;
  public ordersList: any = {};
  private timeInterval: Subscription[] = [];

  ngOnInit(): void {
  }

  public closeWindow = (event: MouseEvent): void => {
    this._bottomSheetRef.dismiss();
    event.preventDefault();
  }

  public checkFields = (): boolean => {
    return !(this.user === undefined ||
      this.delay === undefined ||
      this.priority === undefined);
  }

  public makeOrder = (event: MouseEvent): void => {

    setTimeout(() => this.closeWindow(event), 1000);

    if (!this.checkFields()) {
      return;
    }

    this.orderComplete = true;

    this.order = {
      user: this.user,
      delay: parseInt(this.delay, 10),
      priority: this.priority
    };

    this.http.registerOrder(this.order).subscribe((data: Data) => {
      const id = data.job.id;
      this.ordersList[id] = {...this.order, id};
      this.pollingProcess(id);
    });

  }

  public pollingProcess = (waited: string) => {
    this.timeInterval[waited] = interval(1000)
      .pipe(
        startWith(0),
        switchMap(() => this.http.getOrderStatus(waited))
      ).subscribe(res => {
          if ((res as any).body.id === waited && (res as any).body.status === 'completed') {
            // Unsubscribe
            this.timeInterval[(res as any).body.id].unsubscribe();
            // Sharing
            this.shared.emitValue(this.ordersList[(res as any).body.id]);
          }
        },
        err => console.log('HTTP Error', err));
  }
}
