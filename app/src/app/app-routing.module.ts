import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {P404Component} from './views/p404/p404.component';
import {IndexComponent} from './views/index/index.component';
import {OrderComponent} from './views/order/order.component';
import {HistogramComponent} from './views/histogram/histogram.component';

const routes: Routes = [
  { path: 'order', component: OrderComponent },
  { path: 'histogram', component: HistogramComponent },
  { path: '', component: IndexComponent },
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
