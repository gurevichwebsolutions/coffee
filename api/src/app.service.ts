import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Order } from './order.model';

@Injectable()
export class AppService {

  constructor(@InjectModel('Order') private readonly orderModel: Model<Order>) {}

  // Default
  getHello(): string {
    return 'Hello World!';
  }

  // Add order to DB
  async addOrder(user: string, orderId: string, date = new Date()) {
    console.log(date);
    const newOrder = new this.orderModel({
      user,
      orderId,
      date
    });
    const result = await newOrder.save();
    return result.id as string;
  }

  // Get orders list
  async getOrders() {
    return await this.orderModel.find().exec();
  }
}
