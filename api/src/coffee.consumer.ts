import { OnQueueActive, Process, Processor } from '@nestjs/bull';
import { Job } from 'bull';
import { AppService } from './app.service';

@Processor('coffee')
export class CoffeeConsumer {
    constructor(private readonly appService: AppService) {
    }

    @Process('order')
    async makeOrder(job: Job<unknown>) {
        let order: any;

        if (typeof job.data === 'string') {
            order = JSON.parse(job.data);
            this.appService.addOrder(order.user, job.id.toString()).then(res => console.log('Record added', res));
        }  else {
            console.log('Record doesnt added', job.data);
        }

    }
}
