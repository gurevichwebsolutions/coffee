import { Injectable } from '@nestjs/common';
import { InjectQueue } from '@nestjs/bull';
import {Job, Queue} from 'bull';
import { Order } from './interfaces/order';

@Injectable()
export class CoffeeService {

    constructor(@InjectQueue('coffee') private coffeeQueue: Queue) {}

    createOrder(order: Order){
        return this.coffeeQueue.add('order', JSON.stringify(order),
            {
                delay: order.delay * 1000,
                priority: order.priority
            });
    }

    getJobById(id: string){
        return this.coffeeQueue.getJob(id).then(job => {
            return job.getState().then(state => state);
        });
    }
}
