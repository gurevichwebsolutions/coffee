import {Body, Controller, Get, Post, Query} from '@nestjs/common';
import { AppService } from './app.service';
import { CoffeeService } from './coffee.service';
import { Order } from './interfaces/order';
import { Params } from './interfaces/params';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService,
              private readonly coffeeService: CoffeeService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('order')
  createOrder(@Query() order: Order) {
    console.log(order);
    // Add to queue
    return this.coffeeService.createOrder(order).then(job => {
      return {...order, job};
    });
  }

  @Get('polling')
  pollingProcess(@Query() params: Params) {
    return this.coffeeService.getJobById(params.waited).then(status => {
      return {id: params.waited, status};
    });

  }

  // Doesn't use
  @Post('addorders')
  async addOrder(
      @Body('user') user: string,
      @Body('orderId') orderId: string
  ) {
    const generatedId = await this.appService.addOrder(
        user,
        orderId,
    );
    return { id: generatedId };
  }

  @Get('getorders')
  getOrders(@Query() params: Params) {
    return this.appService.getOrders();
  }
}
