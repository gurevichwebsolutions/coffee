import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BullModule } from '@nestjs/bull';

import { CoffeeService } from './coffee.service';
import { CoffeeConsumer } from './coffee.consumer';
import { MongooseModule } from '@nestjs/mongoose';
import { OrderSchema } from './order.model';

@Module({
  imports: [
    BullModule.forRoot({
      redis: {
        host: 'redis',
        port: 6379,
      },
    }),
    BullModule.registerQueue({
      name:'coffee'
    }),
    MongooseModule.forRoot('mongodb://admin:secure@mongodb:27017/admin'),
    MongooseModule.forFeature([{ name: 'Order', schema: OrderSchema }]),
  ],
  controllers: [AppController],
  providers: [AppService, CoffeeService, CoffeeConsumer],
})
export class AppModule {}
