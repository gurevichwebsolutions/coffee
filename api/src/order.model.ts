import * as mongoose from 'mongoose';

export const OrderSchema = new mongoose.Schema({
    user: { type: String, required: true },
    orderId: { type: String, required: true },
    date: { type: Date, required: true }
});

export interface Order extends mongoose.Document {
    id: string;
    user: string;
    orderId: string;
    date: Date
}
