export interface Order {
  user: string;
  delay: number;
  priority: number;
}
