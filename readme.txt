RUN REDIS AT FIRST TIME:
docker run --name coffee -p 6379:6379 -d ddcca4b8a6f0

START REDIS:
docker start coffee

STOP REDIS:
docker stop coffee

COME IN REDIS:
docker exec -it coffee redis-cli
docker exec -it coffee bash

REDIS-CLI
flushall  - clear all
KEYS * - show all

HELP @string
HELP APPEND

NESTJS
npm run start:dev

=======================
RUN MONGODB AT FIRST TIME:
docker run --name mongodb -p 27017:27017 mongo

RUN MONGODB AT NEXT TIMES:
docker start mongodb

COME IN MONGO
docker exec -it mongodb bash

The container name is mongodb

mongo
use admin
db.createUser({user: "admin", pwd: "secure",  roles: [ { role: "root", db: "admin" } ]})

RUN DOCKER-COMPOSE.YML
docker-compose up

mongodb://admin:secure@localhost:27017/admin

=========================

docker image prune

docker ps
    – просмотр всех контейнеров (подробнее)

docker-compose up --build
    – сборка проекта. Параметр build используется для того, чтобы заставить compose пересоздавать контейнеры.

docker-compose up --build -d
    – запустить проект в фоном режиме

docker stop containerId
    – stop particular container

docker stop $(docker ps -a -q)
    – stop all containers

docker rm containerId
    – remove particular container

docker rm $(docker ps -a -q)
    – remove all containers

=============
docker exec -it rehovot_nginx_1 bash
    – enter to mariaDB container

mysql -u root -p gurevich_mo < var/lib/mysql/db.sql;
